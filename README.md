# JetReviews

## Changelog

### 1.1.1
- FIX: Include Review meta box assets only on required pages.

### 1.1.0
- ADD: Structured Data markup;
- FIX: Conflicts with PHP 7.2.
