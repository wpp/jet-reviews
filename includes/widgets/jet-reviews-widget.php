<?php
/**
 * Class: Jet_Reviews_Banner
 * Name: Banner
 * Slug: jet-banner
 */

namespace Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Repeater;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use Elementor\Utils;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Jet_Reviews_Widget extends Jet_Reviews_Base {

	private $__review_data  = array();

	public function get_name() {
		return 'jet-reviews';
	}

	public function get_title() {
		return esc_html__( 'Review', 'jet-reviews' );
	}

	public function get_icon() {
		return 'eicon-favorite';
	}

	public function get_categories() {
		return array( 'cherry' );
	}

	protected function _register_controls() {

		$css_scheme = apply_filters(
			'jet-reviews/widget/css-scheme',
			array(
				'title'           => '.jet-review__title',
				'fields_box'      => '.jet-review__fields',
				'field'           => '.jet-review__field',
				'field_heading'   => '.jet-review__field-heading',
				'stars'           => '.jet-review__stars',
				'field_value'     => '.jet-review__field-val',
				'progress'        => '.jet-review__progress',
				'progress_bar'    => '.jet-review__progress-bar',
				'progress_value'  => '.jet-review__progress-val',
				'summary'         => '.jet-review__summary-content',
				'summary_data'    => '.jet-review__summary-data',
				'summary_value'   => '.jet-review__summary-val',
				'summary_title'   => '.jet-review__summary-title',
				'summary_content' => '.jet-review__summary-text',
				'summary_legend'  => '.jet-review__summary-legend',
				'summary'         => '.jet-review__summary-content',
			)
		);

		$this->start_controls_section(
			'section_content',
			array(
				'label' => esc_html__( 'Content', 'jet-reviews' ),
			)
		);

		$this->add_control(
			'content_source',
			array(
				'type'       => 'select',
				'label'      => esc_html__( 'Content Source', 'jet-reviews' ),
				'default'    => 'manually',
				'options'    => $this->__review_sources(),
			)
		);

		$this->add_control(
			'review_post_id',
			array(
				'label'       => esc_html__( 'Get Data From Post', 'jet-reviews' ),
				'description' => esc_html__( 'Enter post ID to get data from or leave empy to get data from current post', 'jet-reviews' ),
				'label_block' => true,
				'type'        => Controls_Manager::TEXT,
				'condition'   => array(
					'content_source' => 'post-meta',
				),
			)
		);

		$this->add_control(
			'review_title',
			array(
				'label'   => esc_html__( 'Title', 'jet-reviews' ),
				'type'    => Controls_Manager::TEXT,
				'condition'   => array(
					'content_source'      => 'manually',
				),
			)
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'field_label',
			array(
				'label' => esc_html__( 'Label', 'jet-reviews' ),
				'type' => Controls_Manager::TEXT,
			)
		);

		$repeater->add_control(
			'field_value',
			array(
				'label'   => esc_html__( 'Field Value', 'jet-reviews' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 9,
				'min'     => 1,
				'max'     => 100,
				'step'    => 0.1,
			)
		);

		$repeater->add_control(
			'field_max',
			array(
				'label'   => esc_html__( 'Field Max', 'jet-reviews' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 10,
				'min'     => 1,
				'max'     => 100,
				'step'    => 0.1,
			)
		);

		$this->add_control(
			'review_fields',
			array(
				'type'    => Controls_Manager::REPEATER,
				'fields'  => array_values( $repeater->get_controls() ),
				'default' => array(
					array(
						'field_label' => esc_html__( 'Design', 'jet-reviews' ),
						'field_value' => 9,
						'field_max'   => 10,
					),
				),
				'title_field' => '{{{ field_label }}}',
				'condition'   => array(
					'content_source'      => 'manually',
				),
			)
		);

		$this->add_control(
			'summary_title',
			array(
				'label'     => esc_html__( 'Summary Title', 'jet-reviews' ),
				'default'   => esc_html__( 'Review Summary Title', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXT,
				'condition' => array(
					'content_source'      => 'manually',
				),
			)
		);

		$this->add_control(
			'summary_text',
			array(
				'label'     => esc_html__( 'Summary Text', 'jet-reviews' ),
				'type'      => Controls_Manager::WYSIWYG,
				'default'   => esc_html__( 'Review Summary Description', 'jet-reviews' ),
				'condition' => array(
					'content_source'      => 'manually',
				),
			)
		);

		$this->add_control(
			'summary_legend',
			array(
				'label'     => esc_html__( 'Summary Legend', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXT,
				'default'   => esc_html__( 'Nice!', 'jet-reviews' ),
				'condition' => array(
					'content_source'      => 'manually',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_sdata',
			array(
				'label' => esc_html__( 'Structured Data', 'jet-reviews' ),
			)
		);

		$this->add_control(
			'add_sdata',
			array(
				'label'        => esc_html__( 'Add Structured Data to review box', 'jet-reviews' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'jet-reviews' ),
				'label_off'    => esc_html__( 'No', 'jet-reviews' ),
				'return_value' => 'yes',
				'default'      => '',
			)
		);

		$this->add_control(
			'sdata_item_name',
			array(
				'label'     => esc_html__( 'Reviewd Item Name', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXT,
				'default'   => '',
				'condition' => array(
					'add_sdata'      => 'yes',
					'content_source' => 'manually',
				),
			)
		);

		$this->add_control(
			'sdata_item_image',
			array(
				'label'   => esc_html__( 'Reviewd Item Image', 'jet-reviews' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => array(
					'url' => Utils::get_placeholder_image_src(),
				),
				'condition' => array(
					'add_sdata'      => 'yes',
					'content_source' => 'manually',
				),
			)
		);

		$this->add_control(
			'sdata_item_description',
			array(
				'label'     => esc_html__( 'Reviewd Item Description', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXTAREA,
				'default'   => '',
				'condition' => array(
					'add_sdata'      => 'yes',
					'content_source' => 'manually',
				),
			)
		);

		$default_date = date( 'Y-m-d H:i' );

		$this->add_control(
			'sdata_review_date',
			array(
				'label'     => esc_html__( 'Review Date', 'jet-reviews' ),
				'type'      => Controls_Manager::DATE_TIME,
				'default'   => $default_date,
				'separator' => 'before',
				'condition' => array(
					'add_sdata'      => 'yes',
					'content_source' => 'manually',
				),
			)
		);

		$this->add_control(
			'sdata_review_author',
			array(
				'label'     => esc_html__( 'Review Author Name', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXT,
				'default'   => '',
				'condition' => array(
					'add_sdata'      => 'yes',
					'content_source' => 'manually',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_fields_settings',
			array(
				'label' => esc_html__( 'Fields Settings', 'jet-reviews' ),
			)
		);

		$this->add_control(
			'fields_layout',
			array(
				'label'   => esc_html__( 'Layout', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'points',
				'options' => array(
					'stars'      => esc_html__( 'Stars', 'jet-reviews' ),
					'percentage' => esc_html__( 'Percentage', 'jet-reviews' ),
					'points'     => esc_html__( 'Points', 'jet-reviews' ),
				),
			)
		);

		$this->add_control(
			'fields_stars_position',
			array(
				'label'   => esc_html__( 'Stars Position', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'right',
				'options' => array(
					'right' => esc_html__( 'Right', 'jet-reviews' ),
					'left'  => esc_html__( 'Left', 'jet-reviews' ),
				),
				'condition' => array(
					'fields_layout' => array( 'stars' ),
				),
			)
		);

		$this->add_control(
			'fields_progressbar',
			array(
				'label'        => esc_html__( 'Progressbar', 'jet-reviews' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'jet-reviews' ),
				'label_off'    => esc_html__( 'Hide', 'jet-reviews' ),
				'return_value' => 'yes',
				'default'      => 'yes',
				'condition' => array(
					'fields_layout' => array( 'percentage', 'points' ),
				),
			)
		);

		$this->add_control(
			'fields_value_position',
			array(
				'label'   => esc_html__( 'Values Position', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'above',
				'options' => array(
					'above'  => esc_html__( 'Above Progressbar', 'jet-reviews' ),
					'inside' => esc_html__( 'Inside Progressbar', 'jet-reviews' ),
				),
				'condition' => array(
					'fields_layout'      => array( 'percentage', 'points' ),
					'fields_progressbar' => 'yes'
				),
			)
		);

		$this->add_control(
			'fields_value_alignment',
			array(
				'label'   => esc_html__( 'Values Alignment', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'space-between',
				'options' => array(
					'space-between' => esc_html__( 'Right', 'jet-reviews' ),
					'flex-start'    => esc_html__( 'Left', 'jet-reviews' ),
				),
				'condition' => array(
					'fields_layout'         => array( 'percentage', 'points' ),
					'fields_value_position' => 'above',
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field_heading'] => 'justify-content: {{VALUE}};',
				),
			)
		);

		$this->add_control(
			'fields_label_suffix',
			array(
				'label'     => esc_html__( 'Label Suffix', 'jet-reviews' ),
				'type'      => Controls_Manager::TEXT,
				'condition' => array(
					'content_source'      => 'manually',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_summary_settings',
			array(
				'label' => esc_html__( 'Summary Settings', 'jet-reviews' ),
			)
		);

		$this->add_control(
			'summary_result_position',
			array(
				'label'   => esc_html__( 'Summary Results Block Position', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'right',
				'label_block' => true,
				'options' => array(
					'right'  => esc_html__( 'At right side of summary box', 'jet-reviews' ),
					'bottom' => esc_html__( 'At bottom of summary box', 'jet-reviews' ),
					'left'   => esc_html__( 'At left side of summary box', 'jet-reviews' ),
					'top'    => esc_html__( 'At top of summary box', 'jet-reviews' ),
				),
			)
		);

		$this->add_control(
			'summary_result_width',
			array(
				'label'   => esc_html__( 'Results Block Width', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 30,
					'unit' => '%',
				),
				'range' => array(
					'%' => array(
						'min' => 1,
						'max' => 100,
					),
				),
				'condition' => array(
					'summary_result_position' => array( 'left', 'right' ),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] => 'width: {{SIZE}}%;',
					'{{WRAPPER}} ' . $css_scheme['summary'] => 'width: calc( 100% - {{SIZE}}% );',
				),
			)
		);

		$this->add_control(
			'summary_layout',
			array(
				'label'   => esc_html__( 'Summary Average Layout', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'points',
				'options' => array(
					'stars'      => esc_html__( 'Stars', 'jet-reviews' ),
					'percentage' => esc_html__( 'Percentage', 'jet-reviews' ),
					'points'     => esc_html__( 'Points', 'jet-reviews' ),
				),
			)
		);

		$this->add_control(
			'summary_progressbar',
			array(
				'label'        => esc_html__( 'Progressbar', 'jet-reviews' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'jet-reviews' ),
				'label_off'    => esc_html__( 'Hide', 'jet-reviews' ),
				'return_value' => 'yes',
				'default'      => '',
				'condition' => array(
					'summary_layout' => array( 'percentage', 'points' ),
				),
			)
		);

		$this->add_control(
			'summary_value_position',
			array(
				'label'   => esc_html__( 'Values Position', 'jet-reviews' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'above',
				'options' => array(
					'above'  => esc_html__( 'Above Progressbar', 'jet-reviews' ),
					'inside' => esc_html__( 'Inside Progressbar', 'jet-reviews' ),
				),
				'condition' => array(
					'summary_layout'      => array( 'percentage', 'points' ),
					'summary_progressbar' => 'yes'
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_review_title_style',
			array(
				'label'      => esc_html__( 'Title', 'jet-reviews' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			)
		);

		$this->add_control(
			'title_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['title'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'title_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['title'],
			)
		);

		$this->add_control(
			'title_bg_color',
			array(
				'label' => esc_html__( 'Background Color', 'jet-reviews' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['title'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'title_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['title'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'title_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['title'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'title_alignment',
			array(
				'label'   => esc_html__( 'Alignment', 'jet-reviews' ),
				'type'    => Controls_Manager::CHOOSE,
				'default' => 'left',
				'options' => array(
					'left'    => array(
						'title' => esc_html__( 'Left', 'jet-reviews' ),
						'icon'  => 'fa fa-align-left',
					),
					'center' => array(
						'title' => esc_html__( 'Center', 'jet-reviews' ),
						'icon'  => 'fa fa-align-center',
					),
					'right' => array(
						'title' => esc_html__( 'Right', 'jet-reviews' ),
						'icon'  => 'fa fa-align-right',
					),
				),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['title'] => 'text-align: {{VALUE}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'title_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'placeholder'    => '1px',
				'selector'       => '{{WRAPPER}} ' . $css_scheme['title'],
			)
		);

		$this->add_responsive_control(
			'title_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['title'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_review_fields_style',
			array(
				'label'      => esc_html__( 'Fields', 'jet-reviews' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			)
		);

		$this->add_control(
			'fields_box_style',
			array(
				'label'     => esc_html__( 'Fields Box', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'fields_box_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'placeholder'    => '1px',
				'selector'       => '{{WRAPPER}} ' . $css_scheme['fields_box'],
			)
		);

		$this->add_responsive_control(
			'fields_box_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['fields_box'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'fields_box_padding',
			array(
				'label'      => esc_html__( 'Fields Box Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['fields_box'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'fields_label_style',
			array(
				'label'     => esc_html__( 'Label', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'field_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'field_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['field'],
			)
		);

		$this->add_control(
			'fields_value_style',
			array(
				'label'     => esc_html__( 'Value', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'field_value_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field_value'] => 'color: {{VALUE}}',
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress_value'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'field_value_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['field_value'] . ', {{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress_value'],
			)
		);

		$this->add_control(
			'field_bg_color',
			array(
				'label' => esc_html__( 'Background Color', 'jet-reviews' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'field_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['field'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'field_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['field'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'field_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'placeholder'    => '1px',
				'selector'       => '{{WRAPPER}} ' . $css_scheme['field'],
			)
		);

		$this->add_responsive_control(
			'field_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['field'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'fields_stars_style',
			array(
				'label'     => esc_html__( 'Stars Rating', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_responsive_control(
			'fields_stars_size',
			array(
				'label'   => esc_html__( 'Stars Size', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 16,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 10,
						'max' => 100,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['stars'] => 'font-size: {{SIZE}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'fields_stars_color_empty',
			array(
				'label'  => esc_html__( 'Empty Stars Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['stars'] . ' .jet-review__stars-empty'=> 'color: {{VALUE}}',
				),
			)
		);

		$this->add_control(
			'fields_stars_color_filled',
			array(
				'label'  => esc_html__( 'Filled Stars Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['stars'] . ' .jet-review__stars-filled'=> 'color: {{VALUE}}',
				),
			)
		);

		$this->add_control(
			'fields_stars_gap',
			array(
				'label'   => esc_html__( 'Stars Gap', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 0,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 0,
						'max' => 10,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['stars'] . ' i' => 'margin: 0 calc( {{SIZE}}px/2 );',
				),
			)
		);

		$this->add_control(
			'fields_progress_style',
			array(
				'label'     => esc_html__( 'Progress Styles', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_responsive_control(
			'fields_progress_height',
			array(
				'label'   => esc_html__( 'Progress Height', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 30,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 10,
						'max' => 100,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress_bar'] => 'height: {{SIZE}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'fields_progress_canvas_color',
			array(
				'label'  => esc_html__( 'Progress Canvas Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'fields_progress_canvas_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'selector'       => '{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress'],
			)
		);

		$this->add_control(
			'fields_progress_bar_color',
			array(
				'label'  => esc_html__( 'Progress Bar Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress_bar'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'fields_progress_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['field'] . ' ' . $css_scheme['progress'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'fields_progress_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['field'] . ' ' . $css_scheme['progress'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'fields_progress_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['field'] . ' ' . $css_scheme['progress'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_review_summary_style',
			array(
				'label'      => esc_html__( 'Summary', 'jet-reviews' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			)
		);

		$this->add_control(
			'summary_title_style',
			array(
				'label'     => esc_html__( 'Summary Title', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'summary_title_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_title'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'summary_title_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['summary_title'],
			)
		);

		$this->add_control(
			'summary_content_style',
			array(
				'label'     => esc_html__( 'Summary Content', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'summary_content_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_content'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'summary_content_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_3,
				'selector' => '{{WRAPPER}} ' . $css_scheme['summary_content'],
			)
		);

		$this->add_control(
			'summary_box_style',
			array(
				'label'     => esc_html__( 'Summary Box', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'summary_bg_color',
			array(
				'label' => esc_html__( 'Background Color', 'jet-reviews' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'summary_alignment',
			array(
				'label'   => esc_html__( 'Alignment', 'jet-reviews' ),
				'type'    => Controls_Manager::CHOOSE,
				'default' => 'left',
				'options' => array(
					'left'    => array(
						'title' => esc_html__( 'Left', 'jet-reviews' ),
						'icon'  => 'fa fa-align-left',
					),
					'center' => array(
						'title' => esc_html__( 'Center', 'jet-reviews' ),
						'icon'  => 'fa fa-align-center',
					),
					'right' => array(
						'title' => esc_html__( 'Right', 'jet-reviews' ),
						'icon'  => 'fa fa-align-right',
					),
				),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['summary'] => 'text-align: {{VALUE}};',
				),
			)
		);

		$this->add_responsive_control(
			'summary_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'summary_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'summary_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'placeholder'    => '1px',
				'selector'       => '{{WRAPPER}} ' . $css_scheme['summary'],
			)
		);

		$this->add_responsive_control(
			'summary_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['summary'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_review_summary_average_style',
			array(
				'label'      => esc_html__( 'Summary Average', 'jet-reviews' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			)
		);

		$this->add_control(
			'summary_average_legend_style',
			array(
				'label'     => esc_html__( 'Legend', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'summary_average_legend_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_legend'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'summary_average_legend_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['summary_legend'],
			)
		);

		$this->add_control(
			'summary_average_value_style',
			array(
				'label'     => esc_html__( 'Value', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_control(
			'summary_average_value_color',
			array(
				'label'  => esc_html__( 'Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_value'] => 'color: {{VALUE}}',
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress_value'] => 'color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			array(
				'name'     => 'summary_average_value_typography',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} ' . $css_scheme['summary_value'] . ', {{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress_value'],
			)
		);

		$this->add_control(
			'summary_average_bg_color',
			array(
				'label'     => esc_html__( 'Background Color', 'jet-reviews' ),
				'type'      => Controls_Manager::COLOR,
				'separator' => 'before',
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'summary_average_alignment',
			array(
				'label'   => esc_html__( 'Alignment', 'jet-reviews' ),
				'type'    => Controls_Manager::CHOOSE,
				'default' => 'center',
				'options' => array(
					'left'    => array(
						'title' => esc_html__( 'Left', 'jet-reviews' ),
						'icon'  => 'fa fa-align-left',
					),
					'center' => array(
						'title' => esc_html__( 'Center', 'jet-reviews' ),
						'icon'  => 'fa fa-align-center',
					),
					'right' => array(
						'title' => esc_html__( 'Right', 'jet-reviews' ),
						'icon'  => 'fa fa-align-right',
					),
				),
			)
		);

		$this->add_responsive_control(
			'summary_average_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary_data'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'summary_average_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary_data'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'summary_average_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'placeholder'    => '1px',
				'selector'       => '{{WRAPPER}} ' . $css_scheme['summary_data'],
			)
		);

		$this->add_responsive_control(
			'summary_average_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'summary_stars_style',
			array(
				'label'     => esc_html__( 'Stars Rating', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_responsive_control(
			'summary_stars_size',
			array(
				'label'   => esc_html__( 'Stars Size', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 16,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 10,
						'max' => 100,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['stars'] => 'font-size: {{SIZE}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'summary_stars_color_empty',
			array(
				'label'  => esc_html__( 'Empty Stars Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['stars'] . ' .jet-review__stars-empty'=> 'color: {{VALUE}}',
				),
			)
		);

		$this->add_control(
			'summary_stars_color_filled',
			array(
				'label'  => esc_html__( 'Filled Stars Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['stars'] . ' .jet-review__stars-filled'=> 'color: {{VALUE}}',
				),
			)
		);

		$this->add_control(
			'summary_stars_gap',
			array(
				'label'   => esc_html__( 'Stars Gap', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 0,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 0,
						'max' => 10,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['stars'] . ' i' => 'margin: 0 calc( {{SIZE}}px/2 );',
				),
			)
		);

		$this->add_control(
			'summary_progress_style',
			array(
				'label'     => esc_html__( 'Progress Styles', 'jet-reviews' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			)
		);

		$this->add_responsive_control(
			'summary_progress_height',
			array(
				'label'   => esc_html__( 'Progress Height', 'jet-reviews' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => array(
					'size' => 30,
					'unit' => 'px',
				),
				'range' => array(
					'px' => array(
						'min' => 10,
						'max' => 100,
					),
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress_bar'] => 'height: {{SIZE}}{{UNIT}};',
				),
			)
		);

		$this->add_control(
			'summary_progress_canvas_color',
			array(
				'label'  => esc_html__( 'Progress Canvas Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			array(
				'name'           => 'summary_progress_canvas_border',
				'label'          => esc_html__( 'Border', 'jet-reviews' ),
				'selector'       => '{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress'],
			)
		);

		$this->add_control(
			'summary_progress_bar_color',
			array(
				'label'  => esc_html__( 'Progress Bar Color', 'jet-reviews' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => array(
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				),
				'selectors' => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress_bar'] => 'background-color: {{VALUE}}',
				),
			)
		);

		$this->add_responsive_control(
			'summary_progress_padding',
			array(
				'label'      => esc_html__( 'Padding', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary_data'] . ' ' . $css_scheme['progress'] => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'summary_progress_margin',
			array(
				'label'      => esc_html__( 'Margin', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%', 'em' ),
				'selectors'  => array(
					'{{WRAPPER}} '  . $css_scheme['summary_data'] . ' ' . $css_scheme['progress'] => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->add_responsive_control(
			'summary_progress_border_radius',
			array(
				'label'      => esc_html__( 'Border Radius', 'jet-reviews' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => array( 'px', '%' ),
				'selectors'  => array(
					'{{WRAPPER}} ' . $css_scheme['summary_data'] . ' ' . $css_scheme['progress'] => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				),
			)
		);

		$this->end_controls_section();

	}

	protected function render() {

		$this->__context = 'render';

		$this->__open_wrap();

		$this->__average_data = array(
			'val'  => array(),
			'max'  => array(),
			'calc' => 0,
		);

		$items = $this->__prepare_data();

		include $this->__get_global_template( 'index' );

		$this->structured_data();

		$this->__close_wrap();

	}

	/**
	 * Add structured data markup to review
	 *
	 * @return [type] [description]
	 */
	public function structured_data() {
		$settings = $this->get_settings();

		if ( empty( $settings['add_sdata'] ) ) {
			return;
		}

		$content_source = isset( $settings['content_source'] ) ? $settings['content_source'] : 'manually';

		switch ( $content_source ) {
			case 'manually':
				jet_reviews_tools()->render_structured_data( $this->get_sdata_from_settings() );
				break;

			case 'post-meta':
				jet_reviews_tools()->render_structured_data( $this->get_sdata_from_meta() );
				break;

			case 'wp-review':
				_e( 'Structured data supported only for Post Meta and Manually Input sources', 'jet-review' );
				break;
		}

	}

	public function get_sdata_from_settings() {

		$settings = $this->get_settings();

		$map = array(
			'item_name'     => 'sdata_item_name',
			'item_image'    => 'sdata_item_image',
			'item_desc'     => 'sdata_item_description',
			'review_date'   => 'sdata_review_date',
			'review_author' => 'sdata_review_author',
		);

		$result = array();

		foreach ( $map as $data_key => $settings_key ) {
			$result[ $data_key ] = $settings[ $settings_key ];
		}

		if ( ! empty( $result['item_image'] ) ) {

			$img_id  = absint( $result['item_image']['id'] );
			$img_src = wp_get_attachment_image_src( $img_id, 'full' );

			$result['item_image'] = array(
				'url'    => $img_src[0],
				'width'  => $img_src[1],
				'height' => $img_src[2],
			);

		}

		$result['item_rating'] = $this->get_review_rating();

		return $result;

	}

	public function get_sdata_from_meta() {

		$settings = $this->get_settings();
		$post_id  = ! empty( $settings['review_post_id'] ) ? absint( $settings['review_post_id'] ) : 0;

		if ( ! $post_id ) {
			$post_id = get_the_ID();
		}

		$meta_map = array(
			'item_name'     => 'jet-review-data-name',
			'item_image'    => 'jet-review-data-image',
			'item_desc'     => 'jet-review-data-desc',
			'review_author' => 'jet-review-data-author-name',
		);

		$result = array();

		foreach ( $meta_map as $data_key => $meta_key ) {
			$result[ $data_key ] = get_post_meta( $post_id, $meta_key, true );
		}

		$result['review_date'] = get_the_date( 'c', $post_id );

		if ( ! empty( $result['item_image'] ) ) {

			$img_id  = absint( $result['item_image'] );
			$img_src = wp_get_attachment_image_src( $img_id, 'full' );

			$result['item_image'] = array(
				'url'    => $img_src[0],
				'width'  => $img_src[1],
				'height' => $img_src[2],
			);

		}

		$result['item_rating'] = $this->get_review_rating();

		return $result;
	}

	public function get_review_rating() {

		$average = $this->__get_average();
		$percent = isset( $average['percent'] ) ? $average['percent'] : 0;

		return round( ( ( $percent * 5 ) / 100 ), 2 );
	}

	/**
	 * Prepare reveiews items.
	 *
	 * @return [type] [description]
	 */
	public function __prepare_data() {

		$settings = $this->get_settings();

		$content_source = isset( $settings['content_source'] ) ? $settings['content_source'] : 'manually';

		switch ( $content_source ) {
			case 'manually':
				add_filter( 'jet-reviews/review-data', array( $this, '__set_manual_data' ), 10, 2 );
				break;

			case 'post-meta':
				add_filter( 'jet-reviews/review-data', array( $this, '__set_meta_data' ), 10, 2 );
				break;

			case 'wp-review':
				add_filter( 'jet-reviews/review-data', array( $this, '__set_wp_review_data' ), 10, 2 );
				break;
		}

		$this->__review_data = apply_filters( 'jet-reviews/review-data', array(), $settings, $this );

	}

	/**
	 * Get progress bar HTML markup
	 *
	 * @return string
	 */
	public function __get_progressbar( $value = 0, $max = 10, $label = 'above', $type = 'points' ) {

		$value_label = '';

		if ( 'inside' === $label ) {
			$value_label = ( 'points' === $type ) ? $value : round( ( 100 * $value ) / $max, 0 ) . '%';
		}

		return sprintf(
			'<div class="jet-review__progress"><div class="jet-review__progress-bar" style="width:%1$s%%"><div class="jet-review__progress-val">%2$s</div></div></div>',
			round( ( $value * 100 ) / $max ),
			$value_label
		);

	}

	/**
	 * Get stars rating HTML markup
	 *
	 * @return string
	 */
	public function __get_stars( $value = 0, $max = 10 ) {

		$width   = round( ( 100 * $value ) / $max, 3 );
		$stars_f = str_repeat( '<i class="fa fa-star" aria-hidden="true"></i>', 5 );
		$stars_e = str_repeat( '<i class="fa fa-star-o" aria-hidden="true"></i>', 5 );

		return sprintf(
			'<div class="jet-review__stars"><div class="jet-review__stars-filled" style="width:%1$s%%">%3$s</div><div class="jet-review__stars-empty" style="width:%2$s%%">%4$s</div><div class="jet-review__stars-adjuster">%4$s</div></div>',
			$width,
			100 - $width,
			$stars_f,
			$stars_e
		);

	}

	/**
	 * Calc average review value
	 *
	 * @return array
	 */
	public function __get_average() {

		if ( isset( $this->__average_data['valid'] ) ) {
			return $this->__average_data;
		}

		$default = array(
			'valid'   => true,
			'val'     => 0,
			'max'     => 10,
			'percent' => 0,
		);

		$data   = $this->__get_review_data();
		$fields = $data['review_fields'];

		if ( empty( $fields ) ) {
			return $default;
		}

		$all_max = array();
		$totals  = array();

		foreach ( $fields as $field ) {

			$val = isset( $field['field_value'] ) ? floatval( $field['field_value'] ) : 0;
			$max = isset( $field['field_max'] ) ? floatval( $field['field_max'] ) : 10;

			if ( ! $max ) {
				continue;
			}

			$totals[]  = round( ( 100 * $val ) / $max, 2 );
			$all_max[] = $max;

		}

		$average = round( array_sum( $totals ) / count( $totals ), 2 );
		$all_max = array_unique( $all_max );
		$valid   = 1 === count( $all_max );

		return $this->__average_data = array(
			'valid'   => $valid,
			'val'     => ( $valid ? round( $average * ( $all_max[0] / 100 ), 2 ) : $average ),
			'max'     => ( $valid ? $all_max[0] : 100 ),
			'percent' => $average,
		);
	}

	/**
	 * Return required review fields list
	 *
	 * @return array
	 */
	public function __get_review_fields() {
		return array(
			'review_title',
			'review_fields',
			'summary_title',
			'summary_text',
			'summary_legend',
		);
	}

	/**
	 * Return review data
	 *
	 * @return array
	 */
	public function __get_review_data() {
		return $this->__review_data;
	}

	/**
	 * Set manually entered data if it selected in settings.
	 *
	 * @param  array $settings Widget settings.
	 * @return array
	 */
	public function __set_manual_data( $data, $settings ) {

		foreach ( $this->__get_review_fields() as $field ) {
			$data[ $field ] = isset( $settings[ $field ] ) ? $settings[ $field ] : false;
		}

		return $data;
	}

	/**
	 * Get review data from post meta
	 *
	 * @param  [type] $data     [description]
	 * @param  [type] $settings [description]
	 * @return [type]           [description]
	 */
	public function __set_meta_data( $data, $settings ) {

		$meta_keys = array(
			'jet-review-title',
			'jet-review-items',
			'jet-review-summary-title',
			'jet-review-summary-text',
			'jet-review-summary-legend',
		);

		$post_id = ! empty( $settings['review_post_id'] ) ? absint( $settings['review_post_id'] ) : 0;

		if ( ! $post_id ) {
			$post_id = get_the_ID();
		}

		foreach ( array_combine( $this->__get_review_fields(), $meta_keys ) as $field => $meta_key ) {
			$data[ $field ] = get_post_meta( $post_id, $meta_key, true );
		}

		return $data;

	}

	public function __set_wp_review_data( $data, $settings ) {

		$post_id = ! empty( $settings['review_post_id'] ) ? absint( $settings['review_post_id'] ) : 0;

		if ( ! $post_id ) {
			$post_id = get_the_ID();
		}

		$data['review_title']   = get_post_meta( $post_id, 'wp_review_heading', true );
		$data['summary_legend'] = '';
		$data['summary_title']  = get_post_meta( $post_id, 'wp_review_desc_title', true );
		$data['summary_text']   = get_post_meta( $post_id, 'wp_review_desc', true );

		$type = get_post_meta( $post_id, 'wp_review_type', true );
		$max  = 5;

		switch ( $type ) {
			case 'star':
				$max = 5;
				break;

			case 'percentage':
				$max = 100;
				break;

			case 'point':
				$max = 10;
				break;
		}

		$items = array();
		$meta  = get_post_meta( $post_id, 'wp_review_item', true );

		if ( ! empty( $meta ) ) {
			foreach ( $meta as $item ) {
				$items[] = array(
					'field_label' => isset( $item['wp_review_item_title'] ) ? $item['wp_review_item_title'] : '',
					'field_value' => isset( $item['wp_review_item_star'] ) ? $item['wp_review_item_star'] : 0,
					'field_max'   => $max,
				);
			}
		}

		$data['review_fields'] = $items;

		return $data;

	}

	public function __review_sources() {

		$default = array(
			'manually'  => esc_html__( 'Manually Input', 'jet-reviews' ),
			'post-meta' => esc_html__( 'Post Meta', 'jet-reviews' ),
		);

		if ( $this->__has_wp_review() ) {
			$default['wp-review'] = esc_html__( 'WP Review Data', 'jet-reviews' );
		}

		return apply_filters( 'jet-reviews/widget/review-sources', $default );

	}

	/**
	 * Check if WP Review plugin is installed
	 *
	 * @return bool
	 */
	public function __has_wp_review() {
		return defined( 'WP_REVIEW_PLUGIN_VERSION' );
	}

}
