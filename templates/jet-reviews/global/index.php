<?php
/**
 * Main review template
 */
?>
<div class="jet-review"><?php
	include $this->__get_global_template( 'title' );
	include $this->__get_global_template( 'fields' );
	include $this->__get_global_template( 'summary' );
?></div>